
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">{{$label}}: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="select2 custom-select" style="width: 100%" {{ isset($notrequired)? "" : "required" }}  id="{{$label_name}}" name="{{$label_name}}">
            <option value="">Seleccione...</option>
            @foreach($opciones as $opcion)
                <option @if(isset($value) && $value==$opcion->id) {{'selected'}} @endif value="{{$opcion->id}}">{{isset($opcion->nombre)? $opcion->nombre:$opcion->name}}</option>
            @endforeach
        </select>
    </div>
</div>
