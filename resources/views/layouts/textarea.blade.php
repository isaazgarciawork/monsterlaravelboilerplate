<div class="form-group">
    <label>{{$label_name}} :</label>
    <textarea class="form-control" id="{{$obj_id}}" name="{{$obj_id}}" rows="5" {{isset($readonly)? "readonly":''}}>{{isset($value)? $value:''}}</textarea>
</div>
