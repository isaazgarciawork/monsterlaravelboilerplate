
<div class="form-group row">
    <label  for="{{$label_name}}" class="col-2 col-form-label">{{$label}}: </label>
    <div class="col-10">
        <select {{isset($readonly)? "disabled":''}} class="select2 mb-2 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Seleccione..." {{ isset($notrequired)? "" : "required" }}  id="{{$label_name}}" name="{{$label_name}}">

            <option value="08">8 AM</option>
            <option value="09">9 AM</option>
            <option value="10">10 AM</option>
            <option value="11">11 AM</option>
            <option value="12">12 AM</option>
            <option value="13">1 PM</option>
            <option value="14">2 PM</option>
            <option value="15">3 PM</option>
            <option value="16">4 PM</option>
            <option value="17">5 PM</option>
            <option value="18">6 PM</option>
            <option value="19">7 PM</option>
            <option value="20">8 PM</option>

        </select>
    </div>
</div>
