<div class="form-group row">
    <label for="{{$obj_id}}" class="col-2 col-form-label"> {{isset($label_name)? $label_name:title_case(str_replace('_', ' ', $obj_id))}} :</label>
    <div class="col-10">
        <input class="form-control" type="{{isset($type)? $type:'text'}}"  id="{{$obj_id}}" name="{{$obj_id}}" {{isset($readonly)? "readonly":''}} value="{{isset($value)? $value:''}}" >
    </div>
</div>
