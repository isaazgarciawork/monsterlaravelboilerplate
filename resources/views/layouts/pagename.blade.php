<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">{{$title}}</h3>
        @if(isset($name))
            @if($name!=="Inicio")
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                    <li class="breadcrumb-item active">{{$name}}</li>
                </ol>
            @endif
        @endif
    </div>
    @if(isset($page))
        <div class="col-md-6 col-4 align-self-center">
            <a href="/{{$page}}/create" class="btn pull-right btn-success"><i class="mdi mdi-plus-circle"></i> Crear Nuevo</a>

        </div>
    @endif
</div>
