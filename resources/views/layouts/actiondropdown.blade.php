<div class="btn-group">
    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Acción
    </button>
    <div class="dropdown-menu">
        @if(!isset($notshow))
            <a class="dropdown-item" href="/{{$label}}/{{$id}}">Ver</a>
        @endif
        @if(!isset($notedit))
            <a class="dropdown-item" href="/{{$label}}/{{$id}}/edit">Editar</a>
        @endif
        @if(!isset($notdelete))
            <div class="dropdown-divider"></div>

            <form  action="/{{$label}}/{{$id}}" method="post">
                @csrf
                @method('DELETE')
                <input type="submit" id="deletebutton" class="dropdown-item" value="Borrar">
            </form>
        @endif
    </div>
