<div class="card card-inverse card-{{$color}}">
    <div class="box text-center">
        <h1 class="font-light text-white">{{$value}}</h1>
        <h6 class="text-white">{{$info}}</h6>
    </div>
</div>
