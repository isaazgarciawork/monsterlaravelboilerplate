@extends('layouts.layout')
@section('content')
    @include('layouts.pagename',['title'=>$pluralModel,'name'=>$pluralModel,'page'=>strtolower($pluralModel)])
    @include('layouts.messages')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="display mt-5  nowrap table table-hover table-striped table-bordered " cellspacing="0" width="99%">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(count($elementos) > 0  )
                                @foreach($elementos as $elemento)
                                    <tr>
                                        <td>{{$elemento->name}}</td>
                                        <td>{{$elemento->email}}</td>

                                        <td width="10%">
                                            <div class="row ">
                                                <div class="col-md-6 text-center">
                                                    <a href="/{{strtolower($pluralModel) . "/" .$elemento->id . "/edit"}}"><button class="btn btn-sm btn-success btn-block"><i class="fa fa-edit"></i></button> </a>
                                                </div>
                                                <div class="col-md-6 text-center ">
                                                    <form  action="{{strtolower($pluralModel) . "/" .$elemento->id }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <a><button type="submit" class="btn btn-sm btn-danger btn-block"><i class="fa fa-trash"></i></button> </a>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('extra_scripts')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            } );
        });
    </script>
@endpush
