@extends('layouts.layout')
@section('content')
    @include('layouts.pagename',['title'=>isset($elemento)? 'Editar '.$singularModel: 'Crear' ,'name'=>$singularModel])

    <div class="card">
        <form action="/{{ strtolower($pluralModel) . (isset($elemento)? "/".$elemento->id :"") }}" method="post">
            @csrf
            @if(isset($elemento))
                @method('PUT')
            @endif
            <div class="card-header">
                {{$singularModel}}
            </div>
            <div class="card-body">
                @include('include.errorAlert')
                @include('layouts.textfield',['obj_id'=>'name',"label_name"=>"Nombre","value"=>isset($elemento)? $elemento->name:""])
                @include('layouts.textfield',['obj_id'=>'email',"label_name"=>"Correo","value"=>isset($elemento)? $elemento->email:""])
                @if(!isset($elemento))
                    @include('layouts.textfield',['obj_id'=>'password',"label_name"=>"Contraseña","type"=>"password","value"=>isset($elemento)? $elemento->password:""])
                @endif

                <hr>
                <button type="submit" class="btn btn-primary">{{isset($elemento)? 'Actualizar': 'Guardar'}}</button>
            </div>
        </form>
    </div>
@endsection
