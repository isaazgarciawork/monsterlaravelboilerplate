<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
{{--<script src="{{asset('assets/plugins/bootstrap/js/tether.min.js')}}"></script>--}}
<script src="{{asset('assets/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/sidebarmenu.js')}}"></script>
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('assets/js/custom.min.js')}}"></script>

@stack('extra_scripts')


