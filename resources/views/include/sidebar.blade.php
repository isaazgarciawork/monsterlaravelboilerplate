<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="profile-img"> <img src="/assets/images/users/6.jpg" alt="user" /> </div>
            <div class="profile-text"> <a href="/users/{{ Auth::user()->id }}">{{ Auth::user()->name }}</a>
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                @include('layouts.menuitem',['title'=>" Inicio",'url'=>"/home",'icon_fa'=>"fa fa-home"] )
                @include('layouts.menuitem',['title'=>" Usuarios",'url'=>"/users",'icon_fa'=>"fa fa-user"])

            </ul>
        </nav>
    </div>
    {{--<div class="sidebar-footer">--}}
        {{--<a href="/configuracion" class="link" data-toggle="tooltip" title="Configuración"><i class="ti-settings"></i></a>--}}
        {{--<a href="/usuarios/{{ Auth::user()->id }}" class="link" data-toggle="tooltip" title="Perfil"><i class="mdi mdi-account-settings"></i></a>--}}
        {{--<a href="" class="link" data-toggle="tooltip" title="Cerrar Sesión"><i class="mdi mdi-power"></i></a>--}}
    {{--</div>--}}
    <!-- End Bottom points-->
</aside>
