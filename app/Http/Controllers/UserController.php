<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->data["singularModel"] = "User";
        $this->data["pluralModel"] = "Users";
    }

    public function index()
    {
        $this->data["elementos"]=User::all();
        return view('users.index',$this->data);
    }

    public function create()
    {
        return view('users.create',$this->data);
    }

    public function store(Request $request)
    {
        $request->password = md5($request->password);
        User::create($request->all());
        return redirect()->route('users.index')->with('success','Registro creado satisfactoriamente');
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $this->data["elemento"] = User::findOrFail($id);;
        return view('users.create',$this->data);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return redirect()->route('users.index')->with('success','Registro actualizado satisfactoriamente');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
