# monsterLaravelBoilerplate

	This is a MonsterAdmin Laravel Boilerplate
	Includes template, inputs layouts and login interface.
	Link: https://wrappixel.com/demos/admin-templates/monster-admin/landingpage/index.html
	Official Demo: https://wrappixel.com/demos/admin-templates/monster-admin/main/index2.html
	Composer Download: https://getcomposer.org/download/
	Installation:

	git clone project
	Composer install
	Cp .env.example .env
	php artisan key:generate
	start db service and create database named project (or other and edit .env file with credentials)
	php artisan migrate
	php artisan serve

	Enjoy.